using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Characters
{

    public class ThirdPersonUserControl : MonoBehaviour
    {
        static Animator _animator;
        
        public float speed = 6.0f;
        public float jumpForce = 8.0f;
        public float gravity = 20.0f;
        
        public bool isGrounded;
        
        public float HorizontalValue;
        public float VerticalValue;

        private Rigidbody rb;
        private bool isSliding;

        void Start()
        {
            rb = GetComponent<Rigidbody>();
            _animator = GetComponent<Animator>();
        }

        void Update()
        {
            // Movement
            HorizontalValue = Input.GetAxis("Horizontal");
            VerticalValue = Input.GetAxis("Vertical");

            if (HorizontalValue > 0)
            {
                transform.rotation = new Quaternion()
                {
                    eulerAngles = new Vector3(0,90,0)
                };
                _animator.SetBool("isRunning", true);
            }
            else if (HorizontalValue < 0)
            {
                transform.rotation = new Quaternion()
                {
                    eulerAngles = new Vector3(0, -90, 0)
                };
                _animator.SetBool("isRunning", true);
            }
            else
            {
                _animator.SetBool("isRunning", false);
            }
            Vector3 movement = new Vector3(HorizontalValue, 0.0f, 0.0f) * speed;
            rb.velocity = new Vector3(HorizontalValue * speed, rb.velocity.y,rb.velocity.z);


            // Gravity
            if (!isGrounded)
            {
                // gravity
                rb.AddForce(new Vector3(0.0f, -(gravity * 5f), 0.0f));
                _animator.SetBool("jumped", false);

            }
            else
            {
                // jump
                if (Input.GetButtonDown("Jump"))
                {
                    _animator.SetBool("jumped", true);
                    //movement += new Vector3(0.0f, jumpSpeed, 0.0f);
                    rb.AddForce(Vector3.up * jumpForce * 1000);
                }

                // Gravity
                if (isSliding)
                {
                    rb.AddForce(new Vector3(0.0f, -(gravity * 5f), 0.0f));
                    _animator.SetBool("jumped", false);
                }
            }

        }

        private void OnCollisionStay(Collision collision)
        {
            if (collision.gameObject.tag == "Ground")
            {
                isGrounded = true;
                _animator.SetBool("isGrounded", true);
            }
            if(collision.gameObject.tag == "Slide")
            {
                isSliding = true;
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.tag == "Ground")
            {
                isGrounded = false;
                _animator.SetBool("isGrounded", false);
            }
            if (collision.gameObject.tag == "Slide")
            {
                isSliding = false;
            }
        }
    }
}
