﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesCounter : MonoBehaviour
{
    public static int Lives = 3;
    public GameObject LivesTextBox;
    private string Text = "Exam repeats:";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        string hearts = string.Empty;
        
        for (int i = 0; i < Lives; i++)
        {
            hearts += " ❤";
        }
        LivesTextBox.GetComponent<Text>().text = Text + hearts;
    }
}
