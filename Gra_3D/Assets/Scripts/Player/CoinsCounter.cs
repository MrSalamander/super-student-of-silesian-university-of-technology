﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsCounter : MonoBehaviour
{
    public GameObject PointsDisplay;
    public static int PointCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PointsDisplay.GetComponent<UnityEngine.UI.Text>().text = $"ECTS Points: {PointCount} ";
    }


}
