﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DeathScriptBase : MonoBehaviour
{

    protected void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponentInChildren<AudioMenagement>().PlayDeathSound();
            other.gameObject.GetComponentInChildren<AudioMenagement>().StopPlayingTheme();
            LivesCounter.Lives -= 1;
            if(LivesCounter.Lives <= 0)
            {
                Application.LoadLevel(3);
            }
            StartCoroutine(DeathCoroutine());
        }
        
    }

    protected abstract IEnumerator DeathCoroutine();
}
