﻿using Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeEntry : MonoBehaviour
{

    public GameObject PipeEntryObj;
    public bool StoodOn;
    public GameObject PreviousCamera;
    public GameObject Camera_to_TurnOn;
    public GameObject Player;
    public AudioSource AudioSource;

    public GameObject FadeScreen;

    public float Exit_X;
    public float Exit_Y = 0.5f;
    public float Exit_Z;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (StoodOn)
        {
            if (Input.GetButtonDown("GoDown"))
            {
                StartCoroutine(WaitingForPipe());
            }
        }
        
    }

    private IEnumerator WaitingForPipe()
    {
        FadeScreen.SetActive(true);
        AudioSource.Play();
        transform.parent.FindChild("GroundPlate").gameObject.SetActive(false);

        // animate entry
        PipeEntryObj.GetComponentInChildren<Animator>().enabled = true;
        yield return new WaitForSeconds(1.5f);
        FadeScreen.GetComponentInChildren<Animator>().enabled = true;
        yield return new WaitForSeconds(0.495f);
        FadeScreen.GetComponentInChildren<Animator>().enabled = false;
        PipeEntryObj.GetComponentInChildren<Animator>().enabled = false;
        PipeEntryObj.gameObject.SetActive(false);

        // switch cameras
        PreviousCamera.SetActive(false);
        Camera_to_TurnOn.SetActive(true);

        // teleport player & activate Pipe elevator
        Player.transform.position = new Vector3(Exit_X, Exit_Y, Exit_Z);
        PipeEntryObj.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.495f);
        FadeScreen.GetComponentInChildren<Animator>().enabled = true;
        yield return new WaitForSeconds(0.495f);

        FadeScreen.GetComponentInChildren<Animator>().Update(0f); // reset FadeScreen animation
        FadeScreen.GetComponentInChildren<Animator>().enabled = false;

        PipeEntryObj.GetComponentInChildren<Animator>().Update(0f); // reset PipeGoDown animation
        FadeScreen.SetActive(false);
        Debug.Log("FadeScreen off");
        transform.parent.FindChild("GroundPlate").gameObject.SetActive(true);
        Player.GetComponent<ThirdPersonUserControl>().isGrounded = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        StoodOn = true;
    }

    private void OnTriggerExit(Collider other)
    {
        StoodOn = false;
    }


}
