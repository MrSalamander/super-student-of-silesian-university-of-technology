﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxOpen : MonoBehaviour
{
    public GameObject UnopenedBox;
    public GameObject OpenedBox;
    public bool Opened;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (!Opened && other.gameObject.tag == "Player")
        {
            UnopenedBox.SetActive(false);
            OpenedBox.SetActive(true);

            Opened = true;
        }
    }
}
