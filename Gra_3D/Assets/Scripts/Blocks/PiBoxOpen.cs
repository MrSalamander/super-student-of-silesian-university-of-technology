﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiBoxOpen : BoxOpen
{
    public GameObject Mushroom;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private new void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        Mushroom.SetActive(true);
    }
}
