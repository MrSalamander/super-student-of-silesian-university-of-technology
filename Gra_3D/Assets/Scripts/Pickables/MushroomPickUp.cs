﻿
using Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomPickUp : MonoBehaviour
{
    public AudioSource PickupSound;
    public GameObject Mushroom;
    public float JumpIncrease = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    { 
        if (other.gameObject.tag == "Player")
        {
            PickupSound.Play();
            StartCoroutine(GrowUserAndDisableMushroom(other));
        } 
    }

    private IEnumerator GrowUserAndDisableMushroom(Collider other)
    {
        other.transform.localScale += new Vector3(0.2f, 0.2f, 0.2f);
        var script = other.gameObject.GetComponent<ThirdPersonUserControl>();
        script.jumpForce += JumpIncrease;
        GetComponent<BoxCollider>().enabled = false;
        Mushroom.GetComponent<MushroomBehaviour>().GoingRightSpeed = 0.0f;

        yield return new WaitForSeconds(1.0f);
        Mushroom.SetActive(false);
    }
}
