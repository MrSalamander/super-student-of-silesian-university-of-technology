﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverToMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        LivesCounter.Lives = 3;
        StartCoroutine(WaitAndBackToMenu());

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator WaitAndBackToMenu()
    {
        yield return new WaitForSeconds(6.2f);
        Application.LoadLevel(0);

    }
}
